function Vector2(x, y) {
  this.x = x;
  this.y = y;
  this.add = (other) => new Vector2(this.x + other.x, this.y + other.y);
  this.multiply = (n) => new Vector2(this.x * n, this.y * n);
  this.equals = (other) => this.x === other.x && this.y === other.y;
  this.toString = () => "<" + this.x + ", " + this.y + ">";
}

// Create the canvas
let canvas = document.createElement("canvas");
let ctx = canvas.getContext("2d");
canvas.width = 512;
canvas.height = 480;
document.body.appendChild(canvas);

// Background image
let bgReady = false;
let bgImage = new Image();
bgImage.onload = function () {
  bgReady = true;
};
bgImage.src = "./images/background.png";

// Game objects
let heroReady = false;
let monsterReady = false;
let heroImage = new Image();
heroImage.onload = function () {
  heroReady = true;
};
heroImage.src = "./images/hero.png";
let monsterImage = new Image();
monsterImage.onload = function () {
  monsterReady = true;
};
monsterImage.src = "./images/monster.png";
let hero = {
  "speed": 1,
  "scale": new Vector2(heroImage.width, heroImage.height),
  "position": new Vector2(canvas.width / 2, canvas.height / 2),
  "velocity": new Vector2(0, 0),
  "move": function () {
    this.position = this.position.add(this.velocity.multiply(this.speed));
  },
  "colliding": function (other) {
    return hero.position.x <= (other.position.x + 32) &&
      other.position.x <= (hero.position.x + 32) &&
      hero.position.y <= (other.position.y + 32) &&
      other.position.y <= (hero.position.y + 32);
  },
  "draw": function (ctx, img) {
    ctx.drawImage(img, this.position.x, this.position.y);
  },
  "bounce": function (canvas) {
    if (
      (this.position.x > canvas.width - this.scale.x * 2 && this.velocity.x > 0) ||
      (this.position.x < this.scale.x && this.velocity.x < 0)
    ) {
      this.velocity.x *= -1;
    }
    if (
      (this.position.y > canvas.height - this.scale.y * 2 && this.velocity.y > 0) ||
      (this.position.y < this.scale.y && this.velocity.y < 0)
    ) {
      this.velocity.y *= -1;
    }
  }
};
let monster = {
  "scale": new Vector2(monsterImage.width, monsterImage.height),
  "position": new Vector2(0, 0),
  "velocity": new Vector2(0, 0),
  "draw": function (ctx, img) {
    ctx.drawImage(img, this.position.x, this.position.y);
  }
};
let monstersCaught = 0;

// Handle keyboard controls
let keysDown = {};

addEventListener("keydown", function (e) {
  keysDown[e.keyCode] = true;
}, false);

addEventListener("keyup", function (e) {
  delete keysDown[e.keyCode];
}, false);

// Reset the game when the player catches a monster
function reset() {

  // Throw the monster somewhere on the screen randomly
  monster.position = new Vector2(
    32 + (Math.random() * (canvas.width - monster.scale.x * 3)),
    32 + (Math.random() * (canvas.height - monster.scale.y * 3))
  );
}

function vertPress() {
  if (38 in keysDown || 87 in keysDown) {
    return -1;
  } else if (40 in keysDown || 83 in keysDown) {
    return 1;
  } else {
    return 0;
  }
}

function horzPress() {
  if (37 in keysDown || 65 in keysDown) {
    return -1;
  } else if (39 in keysDown || 68 in keysDown) {
    return 1;
  } else {
    return 0;
  }
}

function update() {
  if ((horzPress() !== 0 || vertPress() !== 0)) {
    hero.velocity = new Vector2(horzPress(), vertPress());
    hero.bounce(canvas);
    hero.move();
  }

  if (hero.colliding(monster)) {
    ++monstersCaught;
    reset();
  }

  render();
};

function render() {
  if (bgReady) {
    ctx.drawImage(bgImage, 0, 0);
  }

  if (heroReady) {
    hero.draw(ctx, heroImage);
  }

  if (monsterReady) {
    monster.draw(ctx, monsterImage);
  }

  // Score
  ctx.fillStyle = "rgb(250, 250, 250)";
  ctx.font = "24px Helvetica";
  ctx.textAlign = "left";
  ctx.textBaseline = "top";
  ctx.fillText("Monsters caught: " + monstersCaught, 32, 32);
};

function main() {
  setInterval(update, 1);
}

reset();
main();